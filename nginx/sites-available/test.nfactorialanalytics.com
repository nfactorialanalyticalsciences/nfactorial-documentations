server {
	listen 80 default_server;
	listen [::]:80 default_server;

	server_name test.nfactorialanalytics.com www.test.nfactorialanalytics.com;

	location / {
		root /home/deploy/nGage-Server/ngage/;
        proxy_pass http://localhost:8090;
		proxy_read_timeout  600;
	}
}
